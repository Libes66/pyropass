import type { Metadata } from "next";
import "./../style/app.scss";
import localFont from "next/font/local";

// Подключаем шрифт
const openSans = localFont({
	src: "/OpenSans.ttf",
	variable: "--font-open-sans",
	display: "swap", // Опционально, для лучшей производительности
});
export const metadata: Metadata = {
	title: "PyroPass",
	description:
		"MaxPass – надежный и удобный сервис для хранения паролей от Максим Кулаков. Защитите свои учетные записи и личные данные с помощью нашего безопасного решения, которое шифрует ваши пароли и обеспечивает доступ с любого устройства. MaxPass предлагает простую в использовании платформу с поддержкой автоматического заполнения паролей, генератором надежных паролей и синхронизацией между устройствами. Доверяйте свою безопасность профессионалам и забудьте о проблемах с паролями. Попробуйте MaxPass сегодня и наслаждайтесь спокойствием и удобством!",
	keywords:
		"Максим Кулаков, парольник, хранить пароли, пароли, пиропас, Максим Кулаков PyroPass",
	icons: {
		icon: [
			{
				rel: "icon",
				url: "assets/img/favicon/faicon.png",
			},
			{
				rel: "apple-touch-icon",
				url: "assets/img/favicon/faicon.png",
			},
		],
	},
};

export default function RootLayout({
	children,
}: Readonly<{
	children: React.ReactNode;
}>) {
	return (
		<html lang='en'>
			<body className={openSans.className}>{children}</body>
		</html>
	);
}
