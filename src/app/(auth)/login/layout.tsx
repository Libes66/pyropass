import { ReactNode } from "react";

function layout({ children }: { children: ReactNode }) {
	return (
		<section className='auth-layout'>
			<div className='auth-layout__content'>
				{children}
			</div>
		</section>
	);
}

export default layout;
