"use client";

import React from "react";
import IconSpinner from "@/img/img/icon/spinner.svg";
import styles from "./styles/button.module.scss";

import { cva } from "class-variance-authority";

interface IProps {
	children: React.ReactNode | string;
	onClick?(): void;
	variant?: "default" | "outline" | "primary" | "secondary";
	size?: "default" | "sm" | "lg";
	disabled?: boolean;
	className?: string;
	loading?: boolean;
	type?: "button" | "submit" | "reset";
}

export function Button({
	children,
	variant = "default",
	size = "default",
	disabled = false,
	loading = false,
	onClick = () => {},
	className = "",
}: IProps) {
	const buttonVariants = cva(styles.button, {
		variants: {
			variant: {
				default: styles.default,
				outline: styles.buttonOutline,
				primary: styles.primary,
				secondary: styles.buttonSecondary,
			},
			size: {
				default: styles.medium,
				sm: styles.small,
				lg: styles.large,
			},
			loading: {
				true: styles.buttonLoading,
				false: "",
			},
			disabled: {
				true: styles.buttonDisabled,
				false: "",
			},
		},
		compoundVariants: [
			{
				variant: "primary",
				size: "default",
				loading: false,
				disabled: false,
				className: styles.button,
			},
		],
		defaultVariants: {
			variant: "primary",
			size: "default",
			loading: false,
			disabled: false,
		},
	});
	console.log(variant, size, disabled, className);
	return (
		<button
			className={buttonVariants({
				variant,
				size,
				disabled,
				className,
			})}
			disabled={disabled}
			onClick={onClick}
		>
			{loading ? <IconSpinner /> : children}
		</button>
	);
}
