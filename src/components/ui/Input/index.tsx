import React, { forwardRef } from "react";
import { cva } from "class-variance-authority";
import IconError from "@/img/img/icon/error.svg";
import { FieldError, FieldErrorsImpl, Merge } from "react-hook-form";

import styles from "./styles/index.module.scss";

interface IProps extends React.InputHTMLAttributes<HTMLInputElement> {
	icon?: React.ReactNode;
	disabled?: boolean;
	className?: string;
	error: FieldError | Merge<FieldError, FieldErrorsImpl<any>> | undefined;
	label?: string;
}

const inputVariants = cva(styles.input, {
	variants: {
		disabled: {
			true: styles.inputDisabled,
			false: "",
		},
		error: {
			true: styles.inputError,
			false: "",
		},
	},
	defaultVariants: {
		disabled: false,
		error: false,
	},
});
const Input = forwardRef<HTMLInputElement, IProps>((props, ref) => {
	const { icon, disabled, error = false, label, ...rest } = props;

	return (
		<>
			<div className={styles.input}>
				<div className={styles.input__wrapper}>
					{icon && <div className={styles.icon}>{icon}</div>}
					<input
						className={styles.input__field}
						ref={ref}
						{...rest}
						disabled={disabled}
					/>
				</div>

				{error && (
					<span data-id={error.type} className={styles.input__errorMessage}>
						<IconError />
						{error.message as string}
					</span>
				)}
			</div>
		</>
	);
});

Input.displayName = "Input";
export default Input;
