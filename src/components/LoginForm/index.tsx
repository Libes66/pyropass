"use client";
import React from "react";
import { useForm } from "react-hook-form";
import { Button } from "@/components/ui/Button";
import Input from "@/components/ui/Input";
import IconLogo from "@/img/img/icon/logo/auth.svg";
import style from "./style/index.module.scss";

const LoginForm = () => {
	const {
		register,
		handleSubmit,
		formState: { errors },
		reset,
	} = useForm();

	const { ref: loginRef, ...loginRegister } = register("login", {
		required: "Введите логин",
	});

	const { ref: passwordRef, ...passwordRegister } = register("password", {
		required: "Введите пароль",
		minLength: {
			value: 6,
			message: "Минимальная длина пароля 6 символов",
		},
	});

	return (
		<div className={style.loginForm}>
			<span className={style.loginForm__title}></span>
			<IconLogo className={style.loginForm__logo} />
			<p className={style.loginForm__text}>Добро пожаловать!</p>
			<form
				className={style.loginForm__form}
				id={"max"}
				onSubmit={handleSubmit(_ => reset())}
			>
				<Input
					label={"Логин"}
					error={errors.login}
					ref={loginRef}
					type='email'
					{...loginRegister}
				/>
				<Input
					label={"Пароль"}
					error={errors.password}
					ref={passwordRef}
					type='password'
					maxLength={passwordRegister.maxLength}
					{...passwordRegister}
				/>

				<Button
					className={style.loginForm__button}
					type='submit'
					variant='primary'
					size={"lg"}
				>
					Войти в панель
				</Button>
			</form>
		</div>
	);
};

export default LoginForm;
