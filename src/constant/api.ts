const BASE_URL = process.env.NEXT_PUBLIC_DOMAIN_DEV;

export const API_LOGIN = BASE_URL + "auth/login";
export const API_REGISTER = BASE_URL + "auth/register";

export const API_FOLDER = BASE_URL + "folder";
export const API_PROJECT = BASE_URL + "project";
export const API_FOLDER_FOLDER = BASE_URL + "folder/folder";
