const { withSentryConfig } = require("@sentry/nextjs");
const withSvgr = require("next-plugin-svgr");

/** @type {import('next').NextConfig} */
const nextConfig = {
	// Any other Next.js configurations can go here
};

// Combine SVGR and Sentry configurations
const combinedConfig = withSentryConfig(
	withSvgr(nextConfig), // SVGR is applied first
	{
		// Sentry configuration options
		org: "maxops",
		project: "javascript-nextjs",
		silent: !process.env.CI,
		widenClientFileUpload: true,
		hideSourceMaps: true,
		disableLogger: true,
		automaticVercelMonitors: true,
	},
);

module.exports = combinedConfig;
